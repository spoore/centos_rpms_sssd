---
- name: Set ansible_python_interpreter to /usr/libexec/platform-python
  set_fact:
     ansible_python_interpreter: auto

- name: Define remote_artificats
  set_fact:
    remote_artifacts: /tmp/artifacts
  when: remote_artifacts is not defined

- name: Install testing requirements
  package: name={{ item }} state=present
  with_items:
    - rsync
  when: ansible_pkg_mgr != 'unknown'

- name: Add epel repo
  yum_repository:
    name: epel8
    file: epel8
    description: EPEL Repo of RHEL 8 because RHEL 9 is not available
    baseurl: https://download.fedoraproject.org/pub/epel/8/Everything/x86_64/
    enabled: no
    gpgcheck: no

- name: Install sshpass from epel
  yum:
    name: sshpass
    enablerepo: epel8
    state: present

- name: Install required packages
  dnf:
    name: >
      git, gcc, redhat-rpm-config.noarch, openldap-devel,
      openldap-clients, dbus-tools, make,
      python3-pip, krb5-devel, sssd, sssd-kcm, libsss_sudo,
      ldb-tools, sssd-tools, nss-tools, sssd-dbus, python3-devel

- name: Install 389-ds-base krb5 server and workstation
  dnf:
    name: >
      389-ds-base, krb5-server, krb5-workstation

- name: clone upstream sssd git to run tests
  git:
    repo: "{{ upstream_sssd_git }}"
    dest: "{{ upstream_src_dir }}"
    version: "{{ upstream_git_branch }}"

- name: Update /etc/hosts file with current hostname
  lineinfile:
    path: /etc/hosts
    line: "{{ ansible_default_ipv4.address }} {{ ansible_hostname }}"

- name: Create Multihost configuration files
  blockinfile:
    dest: /tmp/mhc.yaml
    block: |
       windows_test_dir: '/home/Administrator'
       root_password: 'foobar'
       domains:
         - name: testrealm.test
           type: sssd
           hosts:
             - name: {{ ansible_hostname }}
               external_hostname: {{ ansible_hostname }}
               role: master
    create: yes
    marker: ""

- name: Install pytest-multihost and other test dependencies
  pip:
    virtualenv: "{{ venv_dir }}"
    virtualenv_command: python3 -m venv
    requirements: "{{ upstream_src_dir }}/src/tests/multihost/requirements.txt"

- name: Find directory python searches for path
  shell: python3 -m site --user-site
  register: pypath

- name: Create Python path dir
  file:
    path: "{{ pypath.stdout }}"
    state: directory

- name: Create file with sssd testlib path
  copy:
    dest: "{{ pypath.stdout }}/sssdlib.pth"
    content: "{{ upstream_src_dir }}/src/tests/multihost"

- name: create artifacts directory
  file:
    path: "{{ remote_artifacts }}"
    state: directory
    mode: 0755

- name: Test block
  block:
    - name: Execute pytest
      shell: |
         PYTESTMULTIHOST_SSH_TRANSPORT=openssh \
         "{{ venv_dir }}/bin/pytest" -v \
         --junit-xml="{{ remote_artifacts }}/junit.xml" \
         --multihost-config=/tmp/mhc.yaml \
         "{{ test_dir }}" 1> "{{ remote_artifacts }}/test.log"

    - name: Prepare results.yml in STI format
      shell: |
         echo "results:" > "{{ remote_artifacts }}/results.yml"
         IFS=$'\n'
         for i in `awk '/collected /,/=====/' "{{ remote_artifacts }}/test.log" | sed '1d;$d'`
         do
           echo $i | grep -q PASSED && res=pass
           echo $i | grep -q FAILED && res=fail
           echo $i | grep -q ERROR && res=error
           testname = `echo $i |awk -F\:\: '{print $3}' | cut -f1 -d" "`
           echo "- {result: $res, test: $testname}" >> "{{ remote_artifacts }}/results.yml"
         done
         unset IFS

  always:
    - name: copy sssd logs to "{{ remote_artifacts }}"
      command: cp -a /var/log/sssd {{ remote_artifacts }}

    - name: Pull out logs
      synchronize:
        dest: "{{ artifacts }}"
        src: "{{ remote_artifacts }}/"
        mode: pull
        ssh_args: "-o UserKnownHostsFile=/dev/null"
      when: artifacts|default("") != ""
